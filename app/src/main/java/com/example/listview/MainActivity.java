package com.example.listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    ListView listView;
    SimpleAdapter adapter;
    HashMap<String, String> map;
    ArrayList<HashMap<String, String>> mylist;
    String[] jdl; //deklarasi judul iem
    String[] ktr; //deklarasi keterangan item
    String[] img; //deklarasi image item

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.list_item);
        jdl = new String[]{
                "Sampokong", "Waterpark", "Mangkang", "Wonderia", "Gereja Blenduk"
        };
        ktr = new String[]{
                "Tempat Wisata", "Tempat Wisata", "Tempat Wisata", "Tempat Wisata", "Tempat Wisata", "Tempat Wisata" //jumlahnya harus sama dengan jumlah judul
        };
        img = new String[]{
                Integer.toString(R.drawable.ic_android_black_24dp), Integer.toString(R.drawable.ic_weekend_black_24dp), Integer.toString(R.drawable.ic_local_car_wash_black_24dp),
                Integer.toString(R.drawable.ic_videogame_asset_black_24dp), Integer.toString(R.drawable.ic_laptop_black_24dp)
        };
        mylist = new ArrayList<HashMap<String, String>>();
        for (int i = 0; i < jdl.length; i++) {
            map = new HashMap<String, String>();
            map.put("judul", jdl[i]);
            map.put("Keterangan", ktr[i]);
            map.put("Gambar", img[i]);
            mylist.add(map);
        }
        adapter = new SimpleAdapter(this, mylist, R.layout.list_barang,
                new String[]{"judul", "Keterangan", "Gambar"}, new int[]{R.id.txt_judul, (R.id.txt_keterangan), (R.id.img)});
        listView.setAdapter(adapter);
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, adapter.getItem(position).toString(), Toast.LENGTH_SHORT).show();
    }
}

